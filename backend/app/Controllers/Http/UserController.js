'use strict'
const User = use('App/Models/User')
class UserController {
    async login({ auth, request, response }) {
        const { email, password } = request.all()
        await auth.attempt(email, password)

        return response.status(200).json({ status: 'OK' })
    }

    show({ auth, params }) {
        if (auth.user.id !== Number(params.id)) {
            return "You cannot see someone else's profile"
        }
        return auth.user
    }

    async register({ request, response }) {
        const user = await User.create(request._body)
        response.status(200).json(user)
    }
}

module.exports = UserController
