'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Vote = use('App/Models/Vote')
const Votation = use('App/Models/Votation')
var moment = require('moment')

/**
 * Resourceful controller for interacting with votes
 */
class VoteController {
  /**
   * Show a list of all votes.
   * GET votes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new vote.
   * GET votes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new vote.
   * POST votes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const votes = await Vote.create(request._body)
    response.status(200).json(votes)
  }

  /**
   * Display a single vote.
   * GET votes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing vote.
   * GET votes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update vote details.
   * PUT or PATCH votes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a vote with id.
   * DELETE votes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async resume({ request, response, view }) {
    var votations = await Votation.all()
    for (let i in votations.rows) {
      votations.rows[i].qt = (await Vote.query().where('id_votation', '=', votations.rows[i].id).count('* as total'))[0].total
    }
    response.status(200).json(votations)
  }

  async graph({ request, response, view }) {
    var votations = await Votation.all()
    var date = new Date()
    for (let i in votations.rows) {
      votations.rows[i].name = votations.rows[i].description
      votations.rows[i].data = new Array()
      votations.rows[i].data[0] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 00:00:00', moment(date).format("YYYY-MM-DD") + ' 00:59:59']).count('* as total'))[0].total
      votations.rows[i].data[1] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 01:00:00', moment(date).format("YYYY-MM-DD") + ' 01:59:59']).count('* as total'))[0].total
      votations.rows[i].data[2] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 02:00:00', moment(date).format("YYYY-MM-DD") + ' 02:59:59']).count('* as total'))[0].total
      votations.rows[i].data[3] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 03:00:00', moment(date).format("YYYY-MM-DD") + ' 03:59:59']).count('* as total'))[0].total
      votations.rows[i].data[4] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 04:00:00', moment(date).format("YYYY-MM-DD") + ' 04:59:59']).count('* as total'))[0].total
      votations.rows[i].data[5] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 05:00:00', moment(date).format("YYYY-MM-DD") + ' 05:59:59']).count('* as total'))[0].total
      votations.rows[i].data[6] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 06:00:00', moment(date).format("YYYY-MM-DD") + ' 06:59:59']).count('* as total'))[0].total
      votations.rows[i].data[7] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 07:00:00', moment(date).format("YYYY-MM-DD") + ' 07:59:59']).count('* as total'))[0].total
      votations.rows[i].data[8] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 08:00:00', moment(date).format("YYYY-MM-DD") + ' 08:59:59']).count('* as total'))[0].total
      votations.rows[i].data[9] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 09:00:00', moment(date).format("YYYY-MM-DD") + ' 09:59:59']).count('* as total'))[0].total
      votations.rows[i].data[10] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 10:00:00', moment(date).format("YYYY-MM-DD") + ' 10:59:59']).count('* as total'))[0].total
      votations.rows[i].data[11] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 11:00:00', moment(date).format("YYYY-MM-DD") + ' 11:59:59']).count('* as total'))[0].total
      votations.rows[i].data[12] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 12:00:00', moment(date).format("YYYY-MM-DD") + ' 12:59:59']).count('* as total'))[0].total
      votations.rows[i].data[13] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 13:00:00', moment(date).format("YYYY-MM-DD") + ' 13:59:59']).count('* as total'))[0].total
      votations.rows[i].data[14] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 14:00:00', moment(date).format("YYYY-MM-DD") + ' 14:59:59']).count('* as total'))[0].total
      votations.rows[i].data[15] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 15:00:00', moment(date).format("YYYY-MM-DD") + ' 15:59:59']).count('* as total'))[0].total
      votations.rows[i].data[16] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 16:00:00', moment(date).format("YYYY-MM-DD") + ' 16:59:59']).count('* as total'))[0].total
      votations.rows[i].data[17] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 17:00:00', moment(date).format("YYYY-MM-DD") + ' 17:59:59']).count('* as total'))[0].total
      votations.rows[i].data[18] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 18:00:00', moment(date).format("YYYY-MM-DD") + ' 18:59:59']).count('* as total'))[0].total
      votations.rows[i].data[19] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 19:00:00', moment(date).format("YYYY-MM-DD") + ' 19:59:59']).count('* as total'))[0].total
      votations.rows[i].data[20] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 20:00:00', moment(date).format("YYYY-MM-DD") + ' 20:59:59']).count('* as total'))[0].total
      votations.rows[i].data[21] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 21:00:00', moment(date).format("YYYY-MM-DD") + ' 21:59:59']).count('* as total'))[0].total
      votations.rows[i].data[22] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 22:00:00', moment(date).format("YYYY-MM-DD") + ' 22:59:59']).count('* as total'))[0].total
      votations.rows[i].data[23] = (await Vote.query().where('id_votation', '=', votations.rows[i].id).whereBetween('created_at', [moment(date).format("YYYY-MM-DD") + ' 23:00:00', moment(date).format("YYYY-MM-DD") + ' 23:59:59']).count('* as total'))[0].total
    }
    response.status(200).json(votations)
  }
}

module.exports = VoteController
