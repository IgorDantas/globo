'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Stat = use('App/Models/Stat')
const Votation = use('App/Models/Votation')
/**
 * Resourceful controller for interacting with stats
 */
class StatController {
  /**
   * Show a list of all stats.
   * GET stats
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const stat = await Stat.all()

    response.status(200).json(stat)
  }

  /**
   * Render a form to be used for creating a new stat.
   * GET stats/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new stat.
   * POST stats
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    if (request._body.id) {
      const stat = await Stat.query()
        .where('id', request._body.id)
        .update(request._body)
      response.status(200).json(stat)
    } else {
      await Votation.create({ description: '#MaisShampooSedoso #maça' })
      await Votation.create({ description: '#MaisShampooSedoso #pera' })
      await Votation.create({ description: '#MaisShampooSedoso #cabeloslisos' })
      await Votation.create({ description: '#MaisShampooSedoso #rosas' })
      const stat = await Stat.create(request._body)
      response.status(200).json(stat)
    }
  }

  /**
   * Display a single stat.
   * GET stats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stat.
   * GET stats/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update stat details.
   * PUT or PATCH stats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a stat with id.
   * DELETE stats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = StatController
