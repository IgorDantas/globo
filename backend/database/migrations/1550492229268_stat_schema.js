'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatSchema extends Schema {
  up () {
    this.create('stats', (table) => {
      table.increments()
      table.boolean('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('stats')
  }
}

module.exports = StatSchema
