'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VotationSchema extends Schema {
  up () {
    this.create('votations', (table) => {
      table.increments()
      table.string('description', 500)
      table.timestamps()
    })
  }

  down () {
    this.drop('votations')
  }
}

module.exports = VotationSchema
