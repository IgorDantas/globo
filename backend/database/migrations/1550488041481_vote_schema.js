'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VoteSchema extends Schema {
  up () {
    this.create('votes', (table) => {
      table.increments()
      table.integer('id_votation')
      table.timestamps()
    })
  }

  down () {
    this.drop('votes')
  }
}

module.exports = VoteSchema
