'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { message: 'Hello!' }
})

Route.get('votation', 'VotationController.index')
Route.post('vote', 'VoteController.store')

Route.get('status', 'StatController.index')
Route.post('status', 'StatController.store')

Route.get('resumevotes', 'VoteController.resume')
Route.get('graph', 'VoteController.graph')

Route.post('login', 'UserController.login')
Route.post('register', 'UserController.register')

Route.get('dashboard', 'UserController.show').middleware('auth')