# Backend

> Shampoo Cabelo Sedoso

## Installing

``` bash
# 1) install dependencies
npm install

# 2) configure your database (mysql/mariadb) in file(.env)

# 3) migrate database
adonis migration:run

# 4) serve with at http://localhost:3000
npm run start

```
For more detail contact me igordantas91@icloud.com.