# frontend

> Shampoo Cabelo Sedoso

## Build Setup

``` bash
# install dependencies
npm install

# for edit directory api at frontend
/frontend/src/main.js
# line
axios.defaults.baseURL = 'http://127.0.0.1:3333/' 

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## How to use
``` bash
# 1º register in platform
http://localhost:8080/register

# 2º Login
http://localhost:8080/login

# 3º In Dashboard, click in "iniciar campanha"
http://localhost:8080/dashboard

# 4º url share votation
http://localhost:8080/

# 5º vote
## PS: the user can only vote once, to circumvent the validation is necessary to delete the browser's storage
http://localhost:8080/

# 6º supervise
http://localhost:8080/dashboard

# 7º finish vote, click in "encerar campanha"
http://localhost:8080/dashboard

```


For more detail contact me igordantas91@icloud.com.
