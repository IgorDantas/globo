import Vue from 'vue'
import Router from 'vue-router'
import Vote from '@/components/Vote'
import Register from '@/components/admin/Register'
import Login from '@/components/admin/Login'
import Dashboard from '@/components/admin/Dashboard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Vote',
      component: Vote
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
})
