// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import Vuex from 'vuex'
import axios from 'axios'
import VuexPersistence from 'vuex-persist'
import Simplert from 'vue2-simplert-plugin'
import VueApexCharts from 'vue-apexcharts'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
require('vue2-simplert-plugin/dist/vue2-simplert-plugin.css')

Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)
Vue.use(Simplert)
Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})
Vue.use(VueMaterial)

axios.defaults.baseURL = 'http://127.0.0.1:3333/'
Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    count: 0,
    clicks: 0
  },
  mutations: {
    increment: state => state.count++,
    incrementClicks: state => state.clicks++
  },
  actions: {
    increment (context) {
      context.commit('increment')
    },
    incrementClicks (context) {
      context.commit('incrementClicks')
    }
  },
  plugins: [vuexLocal.plugin]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
